#include <iostream>
#include <omp.h>
using namespace std;

int main()
{
#ifndef _OPENMP
    cout << "OpenMP is not supported!" << endl;
    return 0;
#else
    cout << "OpenMP is supported v" << _OPENMP << endl;

    int num_procs = omp_get_num_procs();
    omp_set_num_threads(num_procs);
#pragma omp parallel shared(cout) default(none)
    {
#pragma omp sections
        {
#pragma omp section
            {
                int myid = omp_get_thread_num();
                cout << "Running in thread " << myid << endl;
            }
#pragma omp section
            {
                int myid = omp_get_thread_num();
                cout << "Report from thread " << myid << endl;
            }
#pragma omp section
            {
                int myid = omp_get_thread_num();
                cout << "Message from thread " << myid << endl;
            }
        }
    }
    return 0;
#endif
}

