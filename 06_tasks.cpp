#include <iostream>
#include <omp.h>
using namespace std;

int main()
{
#ifndef _OPENMP
    cout << "OpenMP is not supported!" << endl;
    return 0;
#else
    cout << "OpenMP is supported v" << _OPENMP << endl;

    int arr[87], num_procs, item_per_thread, sum = 0;

    for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); ++i)
        arr[i] = 1;

    num_procs = omp_get_num_procs();
    cout << "Num of processors = " << num_procs << endl;

    item_per_thread = sizeof(arr) / sizeof(arr[0]) / num_procs;

    omp_set_num_threads(num_procs);
#pragma omp parallel shared(cout, num_procs, arr, item_per_thread, sum) default(none)
    {
#pragma omp single
        for (int i = 0; i < num_procs; ++i)
#pragma omp task shared(cout, arr, item_per_thread, sum) default(none)
        {
            for (int j = 0; j < item_per_thread; ++j)
            {
                int myid = omp_get_thread_num();
#pragma omp atomic
                sum += arr[myid*item_per_thread + j];
            }
        }
    }
    cout << sum << endl;
    return 0;
#endif
}

