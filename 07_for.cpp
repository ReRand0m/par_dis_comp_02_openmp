#include <iostream>
#include <omp.h>
using namespace std;

int main()
{
#ifndef _OPENMP
    cout << "OpenMP is not supported!" << endl;
    return 0;
#else
    cout << "OpenMP is supported v" << _OPENMP << endl;

    int arr[87], num_procs, sum = 0;

    for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); ++i)
        arr[i] = 1;

    num_procs = omp_get_num_procs();
    cout << "Num of processors = " << num_procs << endl;

    omp_set_num_threads(num_procs);
#pragma omp parallel shared(cout, num_procs, arr, sum) default(none)
    {
#pragma omp for
        for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); ++i)
        {
#pragma omp atomic
            sum += arr[i];
        }
    }
    cout << sum << endl;
    return 0;
#endif
}

