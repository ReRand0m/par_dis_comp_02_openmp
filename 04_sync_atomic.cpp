#include <iostream>
#include <omp.h>
using namespace std;

int main()
{
#ifndef _OPENMP
    cout << "OpenMP is not supported!" << endl;
    return 0;
#else
    cout << "OpenMP is supported v" << _OPENMP << endl;

    int myid, num_procs, cntr = 0;

    num_procs = omp_get_num_procs(); // получение количества доступных вычислительных ядер

    omp_set_num_threads(num_procs); // явное задание количества потоков

#pragma omp parallel shared(cout, cntr)  private(myid) default(none)
    { // начало параллельной части программы
        myid = omp_get_thread_num();
#pragma omp atomic
        cntr += 1;
#pragma omp critical
        cout <<  myid << ") cntr = " << cntr << endl;

    } // конец параллельной части программы
    cout << cntr << endl;
    return 0;
#endif
}

