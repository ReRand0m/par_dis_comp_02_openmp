#include <iostream>
#include <omp.h>
using namespace std;

int main()
{
#ifndef _OPENMP
    cout << "OpenMP is not supported!" << endl;
    return 0;
#else
    cout << "OpenMP is supported v" << _OPENMP << endl;

    int myid, num_procs, num_threads;

    num_procs = omp_get_num_procs(); // получение количества доступных вычислительных ядер
    cout << "Num of processors = " << num_procs << endl;

    omp_set_num_threads(2); // явное задание количества потоков

    myid = omp_get_thread_num(); // получение номера потока
    num_threads = omp_get_num_threads(); // получение количества работающих потоков
    cout << "Consecutive part, myid = " << myid << ", num of threads  = " << num_threads << endl;

#pragma omp parallel shared(cout)  private(myid, num_threads) default(none)
    { // начало параллельной части программы
        myid = omp_get_thread_num();
        num_threads = omp_get_num_threads();
        cout << "Parallel part, myid = " << myid << ", num of threads  = " << num_threads << endl;

    } // конец параллельной части программы
    return 0;
#endif
}

