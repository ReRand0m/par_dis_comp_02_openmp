#include <iostream>
#include <omp.h>
using namespace std;

int main()
{
#ifndef    _OPENMP
    cout << "OpenMP is not supported!" << endl;
    return 0;
#else
    cout << "OpenMP is supported v" << _OPENMP << endl;

    int myid, num_procs;

    num_procs = omp_get_num_procs(); // получение количества доступных вычислительных ядер

    omp_set_num_threads(num_procs/2); // явное задание количества потоков

#pragma omp parallel shared(cout)  private(myid) default(none)
    { // начало параллельной части программы
        myid = omp_get_thread_num();
#pragma omp critical
        cout << "Parallel part 1, myid = " << myid << endl;
#pragma omp barrier
#pragma omp critical
        cout << "Parallel part 2, myid = " << myid << endl;

    } // конец параллельной части программы
    return 0;
#endif
}

